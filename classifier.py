import os
import sys
import shutil
import numpy
import librosa
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D


# helpers
##############################################

def get_label(base_dir):
  if base_dir.find('chiptunes') > -1:
    return 0
  elif base_dir.find('mallsoft') > -1:
    return 1

  return 2

def extract_mfccs(file_name, n_mfcc, sample_rate):
  y, sr = librosa.load(file_name, sr=sample_rate, res_type='kaiser_fast') 
  mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=n_mfcc)
  return mfccs

def get_mfccs_and_labels(base_dirs, n_mfcc, sample_rate, desired_shape):
  mfccs, labels = [], []
  i = 0

  for base_dir in base_dirs:
    files = os.listdir(base_dir)

    for file in files:
      if i > 0 and i % 100 == 0:
        print(f'gathering features for row: {i}')

      i += 1

      try:
        if not file.endswith('.wav'):
          continue

        file_name = os.path.join(base_dir, file)
        extraction = extract_mfccs(file_name, n_mfcc, sample_rate)
        if extraction.shape != desired_shape:
          continue

        mfccs.append(extraction)
        labels.append(get_label(base_dir))
      except:
        print(sys.exc_info())

  return mfccs, labels

def get_train_test_split(mfccs, labels):
  x_train, x_test, y_train, y_test = [], [], [], []
  length = len(mfccs) if len(mfccs) < len(labels) else len(labels)

  for i in range(length):
    if i % 4 == 0:
      x_test.append(mfccs[i])
      y_test.append(labels[i])
    else:
      x_train.append(mfccs[i])
      y_train.append(labels[i])

  return numpy.array(x_train), numpy.array(x_test), numpy.array(y_train), numpy.array(y_test)

def get_new_model(desired_shape):
  input_shape = (desired_shape[0], desired_shape[1], 1)

  model = Sequential()
  model.add(Conv2D(64, kernel_size=(3, 3), activation='relu', input_shape=input_shape))
  model.add(MaxPooling2D(pool_size=(2, 2)))
  model.add(Dropout(0.25))
  model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2, 2)))
  model.add(Dropout(0.25))
  model.add(Flatten())
  model.add(Dense(256, activation='relu'))
  model.add(Dense(10, activation='softmax'))

  model.compile(
    loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    optimizer=keras.optimizers.Adam(),
    metrics=['accuracy'])

  return model


# model creation
##############################################

base_dirs = [
  'D:\Music\chiptunes-1',
  'D:\Music\mallsoft-1',
  'D:\Music\synthwave-1']

n_mfcc = 40
sample_rate = 11025
desired_shape = (n_mfcc, 108)

mfccs, labels = get_mfccs_and_labels(
  base_dirs,
  n_mfcc,
  sample_rate,
  desired_shape)

x_train, x_test, y_train, y_test = get_train_test_split(
  mfccs,
  labels)

model = None
model_dir_name = 'D:\Music\model.tf'

if os.path.exists(model_dir_name):
  model = keras.models.load_model(model_dir_name)
else:
  print('{} not found'.format(model_dir_name))
  model = get_new_model(desired_shape)


# model training
##############################################

train = False

if train:
  epochs = 50
  x_train = x_train.reshape(-1, desired_shape[0], desired_shape[1], 1)
  early_stop = keras.callbacks.EarlyStopping(monitor='loss', patience=10)

  model.fit(
    x_train,
    y_train,
    epochs=epochs,
    verbose=True,
    callbacks=[early_stop])

  if os.path.exists(model_dir_name):
    shutil.rmtree(model_dir_name)

  model.save(model_dir_name)


# model test
##############################################

try:
  matches = 0
  x_test = x_test.reshape(-1, desired_shape[0], desired_shape[1], 1)
  length = len(x_test)

  for x in range(length):
    if x > 1 and x % 50 == 0:
      print('comparing results for row: {}'.format(x))

    test_x = numpy.array([x_test[x]])
    test_y = y_test[x]
    result_y = model.predict(test_x)
    prediction_y = numpy.argmax(result_y[0])
    
    if test_y == prediction_y:
      matches += 1
  
  if length > 0:
    accuracy = (matches / length) * 100
    print('total: {}, matches: {}, accuracy: {}%'.format(length, matches, accuracy))

except:
  print(sys.exc_info())
