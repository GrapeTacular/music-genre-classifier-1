### What is this for? ###

* simple music genre classifier with tensorflow
* version: 1.0.0

### Python Modules Tested With ###

* python 3.9.5
* tensorflow 2.5.0 
* numpy 1.19.5
* librosa 0.8.1

### How does one set this up? ###

* set up ffmpeg on computer
* generate wav files from other formats with ffmpeg
* example: ffmpeg -i video.mp4 audio.wav
* split wav files into 5 second fragments with ffmpeg
* example: ffmpeg -i audio.wav -f segment -segment_time 5 -c copy target\segment%03d.wav
* install appropriate Python modules
* modify get_label function and base_dirs list to suit musical taste
* run and train in batches as desired, saving model after each run
